#! /usr/bin/bash

while :
do
	while read -r line
	do
		name="$line"
		getent passwd "$name" > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			usermod -U "$name" > /dev/null 2>&1
			smbclient -e "$name" > /dev/null 2>&1
		else
			if [[ "$name" =~ ^[A-Za-z].*$ ]] && [[ "${#name}" -lt 17 ]]; then
				useradd -m "$name" > /dev/null 2>&1
				echo  "$name":"user$name" | chpasswd > /dev/null 2>&1
				(echo "user$name"; echo "user$name") | smbpasswd -s -a "$name" > /dev/null 2>&1
				echo -e "[$name]\ncomment = $name's share\npath=/home/$name\nbrowseable = yes\nread only = no\ncreate mask = 0700\ndirectory mask = 0700\nvalid users = $name avagd\n" >> "/etc/samba/smb.conf" 2>&1
				echo "$name" >> ".everyUserEver.txt" 2>&1
			else
				:
			fi
		fi
	done < "usernames.txt"

	while read -r line
	do
		name="$line"
		if grep -q -w "$name" "usernames.txt"; then
			usermod -U "$name" > /dev/null 2>&1
			smbpasswd -e "$name" > /dev/null 2>&1
		else
			usermod -L "$name" > /dev/null 2>&1
			smbpasswd -d "$name" > /dev/null 2>&1
		fi
	done < ".everyUserEver.txt"
sleep 15m
done
